local Manual_Seed = true -- Set to true if your compiler doesn't support math.randomseed
local Seed = 6 -- Only use this if it set to false

local Messages_Per_Gen = 10 -- How many messages to gen per excution

local Meow_Commonality = 16
local Meoww_Commonality = 1
local Meowww_Commonality = 1
local Meowwww_Commonality = 1

local Question_Rarity = 4
local Comma_Rarity = 4

local Max_Word_In_Sentence = 5
local Max_Sentences_Per_Message = 3
---------------------------------------------------------------------------------------------
--                                      First Run                                          --
---------------------------------------------------------------------------------------------
local Interal_Word_Lib = {}
if Manual_Seed == false then
    math.randomseed() -- Add tick here
else
    math.randomseed(Seed)
end

function Add_Commonality(phase, num)
   for i = 1, num do
      Interal_Word_Lib[#Interal_Word_Lib+1] = phase
   end
end

-- Don't mind me, I am a dirty way of doing this
Add_Commonality("Meow", Meow_Commonality)
Add_Commonality("Meoww", Meoww_Commonality)
Add_Commonality("Meowww", Meowww_Commonality)
Add_Commonality("Meowwww", Meowwww_Commonality)


---------------------------------------------------------------------------------------------
--                                      Logic                                              --
---------------------------------------------------------------------------------------------

function Generate_Discord_Message()
    local Message_String = ""
    local Sentence_Length = math.random(Max_Sentences_Per_Message)
    for i = 1, Sentence_Length do -- Sentence Length
        local Word_Count = math.random(Max_Word_In_Sentence)
        local Sentence = ""

        for o = 1, Word_Count do -- Words In Sentence
            local Word = Interal_Word_Lib[math.random(#Interal_Word_Lib)]
            if o > 1 then
                string.lower(Word)
            elseif o == 1 and Word_Count > 1 then
                if math.random(Comma_Rarity) == 1 then
                    Word = Word..","
                end
            end

            if o == Word_Count then -- Ending
                if math.random(Question_Rarity) == 1 then
                    Word = Word.."?"
                else
                    Word = Word.."."
                end
            end

            Sentence = Sentence.." "..Word
        end

        Message_String = Message_String.." "..Sentence

        if Sentence_Length > 2 and Sentence_Length == i then
            Message_String = Message_String.." Meo-"
        end
    end

    print(Message_String)
end

for i = 1, Messages_Per_Gen do
    Generate_Discord_Message()
end

